import { Router } from "express";
import HistoryController from "../controllers/history.controller";


const router = Router()
	.get('/:id', HistoryController.getById)
	.post('/', HistoryController.createEvent);

export default router;