import express, { Express, NextFunction, Request, Response } from 'express';
import dotenv from 'dotenv';
import { history } from './routes';
import logger from 'morgan';
import cookieParser from 'cookie-parser';
import { db } from './db';

dotenv.config();
process.env.TZ = "Utc";

const app = express();
const port = process.env.PORT || 3001;
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());

app.use('/history', history);

app.use((req, res, next) => {
  res.status(404).send({ message: '404 NotFound' });
});
app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
  console.error(err.stack);
  res.status(500).send({ message: 'Server Error' });
});

db
  .initialize()
  .then(() => {
    app.listen(port, () => {
      console.log(`Server is listening on port ${port}`);
    });
    console.log("Data Source has been initialized!");
  })
  .catch((err) => console.error("Error during Data Source initialization:", err));

