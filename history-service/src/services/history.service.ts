import { db } from '../db';
import { Event } from '../entity/Event';

class HistoryService {
	async getById(id: number, offset: number, limit: number) {
		const eventsRepository = await db.getRepository(Event);
		return eventsRepository.find({ where: { userId: id }, skip: offset, take: limit });
	}

	async createEvent(event: Event) {
		const eventsRepository = await db.getRepository(Event);
		eventsRepository.create(event);
		return (await eventsRepository.save(event)).id;
	}
}

export default new HistoryService();