import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn } from "typeorm";

@Entity()
export class Event {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    userId: number;

    @Column({type: 'enum', enum: ['create', 'update']})
    type: 'create' | 'update';

    @CreateDateColumn()
    createdAt: Date;
}

