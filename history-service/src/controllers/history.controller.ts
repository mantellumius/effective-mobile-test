import { Request, Response, NextFunction } from 'express';
import HistoryService from '../services/history.service';

class HistoryController {
	async getById(req: Request, res: Response, next: NextFunction) {
		try {
			const id = Number(req.params.id);
			let limit = parseInt(req.query.limit as string);
			let page = parseInt(req.query.page as string);
			limit = Number.isNaN(limit) ? 10 : limit;
			page = Number.isNaN(page) ? 1 : page;
			const offset = (page - 1) * limit;
			const history = await HistoryService.getById(id, offset, limit);
			if (history.length === 0)
				return res.status(404).json({ message: 'History not found' });
			res.json(history);
		} catch (e) {
			next(e);
		}
	}


	async createEvent(req: Request, res: Response, next: NextFunction) {
		try {
			const event = req.body;
			const eventId = await HistoryService.createEvent(event);
			res.json(eventId);
		} catch (e) {
			next(e);
		}
	}
}

export default new HistoryController();