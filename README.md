### Test task
## Установка
Для запуска нужен Docker
```
git clone https://gitlab.com/mantellumius/effective-mobile-test
cd effective-mobile-test
docker-compose up
```
## Api
Api истории http://localhost:3001/history/[id]?page={}&limit={}

Api пользователей http://localhost:3000/users

User {
	password,
	username
}