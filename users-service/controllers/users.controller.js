import { db } from '../db/index.js';
import UsersHistoryService from '../services/users-history.service.js';
import UsersService from '../services/users.service.js';

class UsersController {
	async getAllUsers(req, res, next) {
		try {
			const users = await UsersService.getAll();
			res.json(users);
		} catch (e) {
			next(e);
		}
	}

	async createUser(req, res, next) {
		try {
			const { password, username } = req.body;
			if (!password || !username) {
				return res.status(400).send({ message: 'Bad request' });
			}
			let response = await UsersService.create({ password, username });
			res.json(response[0]);
			UsersHistoryService.send(response[0].id, 'create');
		} catch (e) {
			next(e);
		}
	}

	async updateUser(req, res, next) {
		try {
			const id = req.params.id;
			const { username, password } = req.body;
			if (!username || !password || !id) {
				return res.status(400).send({ message: 'Bad request' });
			}
			let response = await UsersService.update({ id, username, password });
			res.json(response);
			UsersHistoryService.send(id, 'update');
		} catch (e) {
			next(e);
		}
	}
}

export default new UsersController();