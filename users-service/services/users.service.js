import { db } from '../db/index.js';

class UsersService {
	/**
	 * Get all users
	 * @returns {Promise<Array>} - Array of all users
	 */
	getAll() {
		return db('users').select('*');
	}

	/**
	 * Create new user
	 * @param {Object} user
	 * @param {string} user.password
	 * @param {string} user.username
	 * @returns {Promise<number[]>} created user id
	 */
	create(user) {
		return db('users')
			.insert(user)
			.returning('id');
	}

	/**
	 * 
	 * @param {Object} user 
	 * @param {string} user.password
	 * @param {string} user.username
	 * @param {number} user.id
	 * @returns {Promise<Object>} updated user
	 */
	update(user) {
		return db('users')
			.update(user)
			.where('id', user.id)
			.returning('*');
	}
}

export default new UsersService();