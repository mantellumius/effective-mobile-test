import { $historyApi } from "../api/history.js";

class UsersHistoryService {
	/**
	 * 
	 * @param {number} userId 
	 * @param {'create'|'update'} type 
	 */
	async send(userId, type) {
		try {
			await $historyApi.post('/history', {userId, type});
		} catch (e) {
			console.log(`Send history error ${e.message}`)
		}
	}
}

export default new UsersHistoryService();