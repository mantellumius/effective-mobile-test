// Update with your config settings.
import dotenv from 'dotenv';
import path from 'node:path';
import { fileURLToPath } from 'node:url';
const __dirname = path.dirname(fileURLToPath(import.meta.url));
dotenv.config({ path: path.resolve(__dirname, '../.env') });

/**
 * @type { Object.<string, import("knex").Knex.Config> }
 */
export default {
	development: {
		client: 'pg',
		connection: process.env.DB_CONNECTION_STRING,
		searchPath: ['knex', 'public'],
		migrations: {
			tableName: 'knex_migrations',
		},
		pool: {
			min: 2,
			max: 10
		}
	},
	production: {
		client: 'pg',
		connection: {
			database: process.env.DB_NAME,
			host: process.env.DB_HOST,
			user: process.env.DB_USER,
			port: process.env.DB_PORT,
			password: process.env.DB_PASSWORD
		},
		pool: {
			min: 2,
			max: 10
		},
		migrations: {
			tableName: 'knex_migrations'
		}
	}
};
