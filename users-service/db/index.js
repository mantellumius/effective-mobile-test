import knex from 'knex';
import knexfile from './knexfile.js';
const nodeEnv = process.env.NODE_ENV ?? 'development';
export const db = knex(knexfile[nodeEnv]);
