
import express from 'express';
import UsersController from '../controllers/users.controller.js';

const router = express.Router();
router.get('/', UsersController.getAllUsers);
router.post('/', UsersController.createUser);
router.put('/:id', UsersController.updateUser);

export default router;
