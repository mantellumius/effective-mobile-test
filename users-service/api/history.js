import axios from "axios";
import dotenv from 'dotenv';
dotenv.config();

export const URL = process.env.HISTORY_SERVICE_URL;
export const API_URL = `${URL}`;

export const $historyApi = axios.create({
	withCredentials: true,
	baseURL: API_URL
});
