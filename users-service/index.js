import express from 'express';
import logger from 'morgan';
import cookieParser from 'cookie-parser';
import dotenv from 'dotenv';
import { users } from './routes/index.js';

dotenv.config();
process.env.TZ = "Utc";

const app = express();
const port = process.env.PORT || 3000;
app.set('port', port);
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use('/users', users);
app.use((req, res, next) => {
	res.status(404).send({message: '404 NotFound'});
});
app.use((err, req, res, next) => {
	console.error(err.stack);
	res.status(500).send({message: 'Server Error'});
});

app.listen(port, () => {
	console.log(`Server is listening on port ${port}`);
});
